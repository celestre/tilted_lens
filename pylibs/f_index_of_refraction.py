#!/usr/bin/python
# coding: utf-8
###################################################################################
# Authors/Contributors: Rafael Celestre
# Rafael.Celestre@esrf.fr
# creation: 13.08.2018
# previously updated: 16.10.2018
# last update: 17.10.2018 (v0.1)
###################################################################################

try:
    from srwlib import SRWLOptT, srwl_uti_ph_en_conv, srwl_uti_read_data_cols
    from uti_math import interp_1d_var
    print(">>> Native SRW found.")

except:
    from oasys_srw.srwlib import SRWLOptT, srwl_uti_ph_en_conv, srwl_uti_read_data_cols
    from oasys_srw.uti_math import interp_1d_var

import numpy as np
import os

# ****************************************************************************
# **********************Auxiliary functions
# ****************************************************************************


def index_of_refraction(_material, _E, _file_path='../../pylibs/materials/'):
    """
    For a given energy range (1000 eV < E < 30.000 eV), this function returns an array composed of the complex index of
    refraction and the attenuation length for a given material from the database. Existing materials are: Al, Be, C,
    Diamond, Ni, Si and SU-8 (SU8 or PMMA).
    :param _material: string (not case sensitive). Existing materials are: Al, Be, C, Diamond, Ni, Si, SiO2 and SU-8 (SU8 or PMMA).
    :param _E: fundamental photon energy [eV]
    :param _file_path: folder where the the .dat files for different materials are saved
    :return complex_index_of_refraction: delta, beta, atten_len
    """
    # TODO: change for xraylib functions
    if (_E > 30000) or (_E < 1000):
        print(">>>> Error: energy out of range. Energies must be in the range 1000 eV < E < 30,000 eV\n")
        return
    try:
        Wavelength = srwl_uti_ph_en_conv(_E, 'eV', 'm')
        if _material.lower() == "al":
            Mat_dir = os.path.join(_file_path,"Al.dat")
        elif _material.lower() == "be":
            Mat_dir = os.path.join(_file_path,"Be.dat")
        elif _material.lower() == "c":
            Mat_dir = os.path.join(_file_path,"C.dat")
        elif _material.lower() == "diamond":
            Mat_dir = os.path.join(_file_path,"Diamond.dat")
        elif _material.lower() == "ni":
            Mat_dir = os.path.join(_file_path,"Ni.dat")
        elif _material.lower() == "si":
            Mat_dir = os.path.join(_file_path,"Si.dat")
        elif _material.lower() == "sio2":
            Mat_dir = os.path.join(_file_path, "SiO2.dat")
        elif (_material.lower() == "su8") or (_material.lower() == "su-8") or (_material.lower() == "pmma"):
            Mat_dir = os.path.join(_file_path,"PMMA.dat")
        Material = srwl_uti_read_data_cols(Mat_dir, " ", 0, -1, 2)
        delta = interp_1d_var(_E, Material[0], Material[1])
        beta  = interp_1d_var(_E, Material[0], Material[2])
        atten_len = Wavelength / (4 * np.pi * beta)

        return delta, beta, atten_len
    except:
        print(">>>> Error: unknown material in the database. Existing materials are: Al, Be, C, Diamond, Ni, Si, SiO2 "
              "and SU-8 (SU8 or PMMA).\n")
        print(Mat_dir)
        return
