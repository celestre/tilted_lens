#!/bin/python#
# coding: utf-8
###################################################################################
# Authors/Contributors: Rafael Celestre
# Rafael.Celestre@esrf.fr
# creation: 22.07.2022
# previously updated: 16.10.2018
# last update: 17.10.2018 (v0.1)
###################################################################################

import numpy as np
from scipy.optimize import curve_fit
from scipy.interpolate import interp2d

# ****************************************************************************
# **********************Auxiliary functions
# ****************************************************************************

def get_averaged_gradient(gradient, f_dir, roi=None, px=30, dg=2, relative=False, silent=False):
    """

    :param gradient:
    :param f_dir:
    :param roi:
    :param px:
    :param dg:
    :param relative:
    :param silent:
    :return:
    """

    if f_dir == 'h':
        mean_ax = 0
        tag = 'hor. grad.'
    if f_dir == 'v':
        mean_ax = 1
        tag = 'ver. grad.'

    if roi is None:
        mean_grad = np.mean(gradient, axis=mean_ax)
        if relative:
            ax = np.linspace(-len(mean_grad) / 2, len(mean_grad) / 2, len(mean_grad))
        else:
            ax = np.linspace(0, len(mean_grad) - 1, len(mean_grad))
    else:
        mean_grad = np.mean(gradient[int(roi[0]-px):int(roi[1]+px), int(roi[2]-px):int(roi[3]+px)], axis=mean_ax)
        if f_dir == 'b' or f_dir == 'h':
            ax = np.linspace(int(roi[2]-px), int(roi[3]+px), len(mean_grad))
        if f_dir == 'b' or f_dir == 'v':
            ax = np.linspace(int(roi[0]-px), int(roi[1]+px), len(mean_grad))

    # left side of the slope
    ax_l = ax[np.argmin(mean_grad)-px:np.argmin(mean_grad)+px]

    mean_grad_l = mean_grad[np.argmin(mean_grad)-px:np.argmin(mean_grad)+px]
    p = np.polyfit(ax_l, mean_grad_l, dg)
    fl = np.zeros(len(mean_grad_l))
    for i in range(dg + 1):
        fl += p[i] * ax_l ** (dg - i)

    # right side of the slope
    ax_r = ax[np.argmax(mean_grad) - px:np.argmax(mean_grad) + px]
    mean_grad_r = mean_grad[np.argmax(mean_grad) - px:np.argmax(mean_grad) + px]
    p = np.polyfit(ax_r, mean_grad_r, dg)
    fr = np.zeros(len(mean_grad_r))
    for i in range(dg + 1):
        fr += p[i] * ax_r ** (dg - i)

    data = {'mean_grad': {'grad': mean_grad, 'ax': ax}}
    data.update({'reduced': {'ax_l': ax_l, 'ax_r': ax_r, 'fl': fl, 'fr': fr}})
    data.update({'points': {'axl': ax_l[np.argmin(fl)], 'lmin': np.amin(fl),
                            'axr': ax_r[np.argmax(fr)], 'rmax': np.amax(fr)}})

    if silent is True:
        import barc4plots.barc4plots as b4pt
        image = b4pt.Image2Plot(fr*1e6, ax_r)
        image.ColorScheme = -2
        image.LineStyle = '-'
        image.sort_class()
        b4pt.plot_1D(image, Enable=False, Hold=False)
        image = b4pt.Image2Plot(fl*1e6, ax_l)
        image.ColorScheme = -2
        image.LineStyle = '-'
        image.sort_class()
        b4pt.plot_1D(image, Enable=False, Hold=True)
        image = b4pt.Image2Plot(mean_grad*1e6, ax)
        image.ColorScheme = 1
        image.LineStyle = '--'
        image.sort_class()
        image.legends = [tag, 'pixel', '$\mu$rad']
        b4pt.plot_1D(image, Enable=True, Hold=True)

    return data


def gauss(x, H, A, x0, sigma):
    return H + A * np.exp(-(x - x0) ** 2 / (2 * sigma ** 2))


def gauss_fit(x, y):
    mean = sum(x * y) / sum(y)
    sigma = np.sqrt(sum(y * (x - mean) ** 2) / sum(y))
    popt, pcov = curve_fit(gauss, x, y, p0=[min(y), max(y), mean, sigma])
    return popt


def fwhm(y, x):
    y = y/np.amax(y)
    x_max = np.argmax(y)
    hwhm_r = -1
    hwhm_l = -1
    k = 0
    while hwhm_r == -1:
        k+=1
        if x_max+k >= x.size:
            break
        if y[x_max+k]<= 0.5:
            hwhm_r = x[x_max+k]

    if hwhm_r == -1:
        hwhm_r = x[-1]
    else:
        hwhm_r = np.interp(0.5, y[x_max+k-3:x_max+k+3].flatten(),x[x_max+k-3:x_max+k+3].flatten())

    k = 0
    while hwhm_l == -1:
        k-=1
        if x_max+k<= 0:
            break
        if y[x_max+k]<=0.5:
            hwhm_l = x[x_max+k]

    if hwhm_l == -1:
        hwhm_l = x[0]
    else:
        hwhm_l = np.interp(0.5, y[x_max + k - 3:x_max + k + 3].flatten(), x[x_max + k - 3:x_max + k + 3].flatten())

    return hwhm_r-hwhm_l


def get_fwhm(image, x, y, coords_x, coords_y):
    f = interp2d(x, y, image, kind='linear')
    if coords_x == ':':
        coords_x = x
        cut = f(coords_x, coords_y)
        return fwhm(cut, coords_x)
    if coords_y == ':':
        coords_y = y
        cut = f(coords_x, coords_y)
        return fwhm(cut, coords_y)