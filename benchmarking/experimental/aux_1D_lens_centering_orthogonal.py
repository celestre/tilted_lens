# imports

# checking we are using the correct python:
import sys
print(sys.executable)
print(sys.version)

import barc4plots.barc4plots as b4pt         # get barc4plots: https://gitlab.esrf.fr/celestre/barc4plots
import fabio as io
from glob import glob
import numpy as np


def main():
    gradient_list = sorted(glob('./1D_Be_R100um_orthogonal/*grad_h.tif'))
    k = 0
    print('> manual ROI selection')

    for grad in gradient_list:
        print(grad)
        if k == 0:
            centre = np.zeros([len(gradient_list), 2])

        gradient = io.open(grad).data
        image = b4pt.Image2Plot(gradient)
        cmean = np.nanmean(gradient)
        cstd = np.std(gradient)
        image.plt_limits = [cmean - 4 * cstd, cmean + 4 * cstd]
        image.ColorScheme = 12
        image.legends = ['', 'x', 'y']
        xi, xf, yi, yf = image.get_ROI_coords(coords='p', roi='r')

        centre[k, 0] = int((yf + yi) / 2)
        centre[k, 1] = int((xf + xi) / 2)

        k+=1

    print(centre)

    np.save('./results/1D_Be_R100um_cen_orthogonal', centre)

if __name__ == '__main__':
    main()
